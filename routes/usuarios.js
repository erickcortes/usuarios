const Usuario = require('../models/usuarios');


module.exports = (app, next) => {
  app.get('/usuario', (req, resp) => {
    const query = req.query ? req.query : {};
    query.empleadosACargo = { $ne: [] }
    Usuario.aggregate([
      {
        $lookup: {
          from: 'users', // Nombre de la colección de empleados (pluralizado)
          localField: 'delegatedUsers',
          foreignField: '_id',
          as: 'delegatedUsers'
        }
      },
      {
        $match: query
      }
    ]).then(r => {
      resp.json(r);
    }, (error) => {
      resp.status(404).send(error);
    }).catch(next);
  });

  app.post('/usuario', (req, resp) => {
    Usuario.create(req.body)
      .then(user => {
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }
      ).catch(next);
  });

  app.put('/usuario/:id', (req, resp) => {
    const body = req.body;
    const email = req.params.id;
    Usuario.updateOne({ correo: email }, { $set: body })
      .then(user => {
        console.log(user)
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }
      ).catch(next);
  });
  
  app.delete('/usuario/:id', (req, resp) => {
    const email = req.params.id;
    Usuario.deleteOne({ email })
      .then(user => {
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }).catch(next);
  });
  return next();
};