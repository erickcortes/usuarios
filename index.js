const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5003
const mongoose = require('mongoose');
const config = require('./config');
const { mongoUrl } = config;
const routes = require('./routes');
const app = express();
const pkg = require('./package.json');
const errorHandler = require('./middleware/error');
const cors = require('cors');


  mongoose.connect(mongoUrl, { useNewUrlParser: true });
    
  app.use(express.json());

  app.use(cors({
    origin: '*',
    methods: ['POST','GET','PUT','DELETE']
  }));

  app.use(express.static(path.join(__dirname, 'public')))
  app.set('views', path.join(__dirname, 'views'))
  app.set('view engine', 'ejs')
  //app.get('/', (req, res) => res.render('pages/index'))
  app.set('config', config);
  app.set('pkg', pkg);

  // Add headers before the routes are defined
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
  

  routes(app, (err) => {
    if (err) {
      throw err;
    }
    // Registro de "middleware" que maneja posibles errores
    app.use(errorHandler); 
  
    app.listen(PORT, () => console.log(`App listening on port ${PORT}`));
  });
  
  